package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Jogo {

	public static void Menu() {
		System.out.println("***************** Jogo 21 *****************");
		System.out.println("Escolha as opções: ");
		System.out.println("1. Jogar modo single-player ");
		System.out.println("0. Sair ");
	}

	public static int validarCarta(String valorCarta) {
		int valorCartaConvertido;
		if (!(valorCarta.equals("As")) && !(valorCarta.equals("Valete")) && !(valorCarta.equals("Dama"))
				&& !(valorCarta.equals("Rei"))) {
			valorCartaConvertido = Integer.parseInt(valorCarta);
		} else {
			if (valorCarta.equals("As")) {
				valorCartaConvertido = 1;
			} else {
				valorCartaConvertido = 10;
			}
		}
		return valorCartaConvertido;
	}

	public static void validarPontos(int totalPontos) {
		if (totalPontos == 21) {
			System.out.println();
			System.out.println("Você ganhou! 21 Pontos!");
		} else {
			System.out.println();
			System.out.println("Você perdeu :/ " + totalPontos + " Pontos!");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String menu = "";
		Scanner ler = new Scanner(System.in);
		String carta;
		int valorCarta;
		int naipeCarta;
		int valorCartaConvertido;
		int totalPontos = 0;
		int cont=0;
		String valorConvertido, naipeConvertido;
		Mesa mesa = new Mesa();
		List<Cartas> listaCartas = new ArrayList();
		Baralho baralho = new Baralho();

		Menu();
		menu = ler.nextLine();

		while (!menu.equals("0")) {
			//System.out.println();
			System.out.println("Pressione enter para pegar uma carta do baralho!");
			menu = ler.nextLine();
			
			valorCarta = mesa.sortearValorCarta();
			naipeCarta = mesa.sortearNaipeCarta();

			
			listaCartas = baralho.embaralhar(valorCarta, naipeCarta);
			
			totalPontos += validarCarta(baralho.getValor(cont));
			
			carta = baralho.getValor(cont) + " de " + baralho.getNaipe(cont);
			
			System.out.println("Sua carta sorteada é: " + carta);
			
			//totalPontos = Calculador.calcularPontos(valorCartaConvertido);
			
			System.out.println("Pontuação: "+totalPontos);
			
			System.out.println();
			//System.out.println("Pressione enter para pegar uma carta do baralho ou 0 para terminar!");
			//menu = ler.nextLine();
			
			if (totalPontos >= 21 || menu.equals("0")) {
				validarPontos(totalPontos);
				return;
			}
			
			cont++;

		}
		System.out.println("Fim do jogo");

	}

}
