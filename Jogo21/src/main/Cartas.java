package main;

import java.util.List;

public class Cartas {
	private String valor;
	private String naipe;
	
	private List<String> listaCarta;
	
	public String getValor() {
		return valor;
	}
	
	public String getNaipe() {
		return naipe;
	}
	
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public void setNaipe(String naipe) {
		this.naipe = naipe;
	}
	
	/*
	private static String [] valores = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valete", "Dama", "Rei"};
	private static String [] naipes = {"Paus", "Ouros", "Copas", "Espadas"};
	
	public static String valores(int index){
		return valores[index];
	}
	
	public static String [] vetorValores(){
		return valores;
	}
	
	public static String naipes(int index){
		return naipes[index];
	}
	
	public static String [] vetorNaipes(){
		return naipes;
	}*/
}
