package main;

import java.util.ArrayList;
import java.util.List;

public class Mesa {
	private int sorteValor, sorteNaipe, tamanhoVetorValor, tamanhoVetorNaipe;
	Baralho baralho = new Baralho();

	public int sortearValorCarta() {
		tamanhoVetorValor = baralho.tamanhoValores();
		sorteValor = Sorteador.sortearValor(tamanhoVetorValor);
		return sorteValor;
	}

	public int sortearNaipeCarta() {
		tamanhoVetorNaipe = baralho.tamanhoNaipes();
		sorteNaipe = Sorteador.sortearNaipe(tamanhoVetorNaipe);
		return sorteNaipe;
	}
}
