package main;

import java.util.ArrayList;
import java.util.List;

public class Baralho {
	
	private String [] valores = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valete", "Dama", "Rei"};
	private String [] naipes = {"Paus", "Ouros", "Copas", "Espadas"};
	List<Cartas> listaCarta = new ArrayList();

	Cartas carta = new Cartas();
	
	int cont=0;
	
	public Baralho() {
		carta.setValor("");
		carta.setNaipe("");
		listaCarta.add(carta);
	}
	
	public int tamanhoValores() {
		return valores.length;
	}
	
	public int tamanhoNaipes() {
		return naipes.length;
	}
	
	public List<Cartas> embaralhar(int valor, int naipe){
		
		for(int i=0; i < listaCarta.size(); i++) {
			if(listaCarta.get(i).getValor().equals(valores[valor]) && listaCarta.get(i).getNaipe().equals(naipes[naipe])) {
				cont++;
			}
		}
		if (cont == 0) {
			carta.setValor(valores[valor]);
			carta.setNaipe(naipes[naipe]);
			listaCarta.add(carta);
		}
		return listaCarta;

	}
	
	public String getValor(int index) {
		return listaCarta.get(index).getValor();
	}
	
	public String getNaipe(int index) {
		return listaCarta.get(index).getNaipe();
	}
}